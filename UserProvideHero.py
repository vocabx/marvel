
import json
import requests
import hashlib
from random import randint

ts =randint(0,1000)
#ts = str(1001)
#ts = str(datetime.datetime.utcnow())
print(ts)

publickey= 'fdbf07e71c484f4d729b1e0f86c2a30e'
privatekey = '202aa57a3cc56c389f9c1127fe30fd43806075b5'
marvelcode = str(ts) + privatekey + publickey

# http://stackoverflow.com/questions/5297448/how-to-get-md5-sum-of-a-string
print(marvelcode)
print(hashlib.md5(marvelcode.encode('utf-8')).hexdigest())
hashvalue = hashlib.md5(marvelcode.encode('utf-8')).hexdigest()
print(hashvalue)

hero = input("Who is your favorite Marvel hero: ")

# Filter the result based on hero name
url_base = 'https://gateway.marvel.com:443/v1/public/characters?name=' + hero + '&'

# Make an API call and store the response
url = url_base + "ts=" +str(ts)+ "&apikey=" + publickey + "&hash=" + hashvalue
print(url)
r = requests.get(url)
print("Status code:", r.status_code)

# Store API response in a variable.
response_dict = r.json()
print("data:", response_dict['data'])

# Explore information about the repositories.
repo_dicts = response_dict['data']
print("Count of records:", len(repo_dicts))

# use the following pattern to pull out names from the nested dictionary
heroename = response_dict['data']['results'][0]['name']
description = response_dict['data']['results'][0]['description']

print("Just a few words about " + heroename + ": " + description)